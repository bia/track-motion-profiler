package plugins.adufour.trackprocessors.speed;

import icy.plugin.abstract_.Plugin;

/**
 * TrackManager plug-in that compute motion and speed statistics from tracks and filter them based
 * on these statistics
 * 
 * @author Alexandre Dufour
 */
public class SpeedProfiler extends Plugin
{
    // old class name, need to preserve it to identify the plugin.
    public SpeedProfiler()
    {
        // nothing to do here, just a container for 2 plugins
    }
}
